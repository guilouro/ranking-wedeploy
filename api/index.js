const express = require('express');
const fetch = require('node-fetch');

const URL = "http://jsuol.com.br/c/monaco/utils/gestor/commons.js?file=commons.uol.com.br/sistemas/esporte/modalidades/futebol/campeonatos/dados/2019/30/dados.json"

const app = express();
const port = process.env.PORT || 3000;

app.all('/', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/', async (req, res) => {
    let data = await fetch(URL).then(r => r.json());
    const unico = data['fases']['2878']['classificacao']['grupo']['Único'];

    const result = unico.map(id => {
        const pontos = data['fases']['2878']['classificacao']['equipe'][id];
        const equipe = data['equipes'][id];

        return {
            "url_logo_equipe": equipe["brasao"],
            "nome_equipe": equipe["nome-comum"],
            "pontos": pontos["pg"]["total"],
            "num_jogos": pontos["j"]["total"],
            "num_vitorias": pontos["v"]["total"],
            "num_empates": pontos["e"]["total"],
            "num_derrotas": pontos["d"]["total"],
            "gols_pro": pontos["gp"]["total"],
            "gols_sofridos": pontos["gc"]["total"],
            "saldo_gols": pontos["sg"]["total"],
        }
    });

    res.json(result)
});

app.listen(port, () => console.log(`Api started on http://localhost:${port}`));

module.exports = app;