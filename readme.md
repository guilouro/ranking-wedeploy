### Tabela Brasileirão We Deploy

Projeto feito para embed de tabela do campeonato brasileiro usando *React*, *Node* e *Docker*, com deploy na *We Deploy*

## Api

Serviço de API que alimenta o front 

```
https://ranking-api.guilouro.now.sh/
```

## App

Serviço para exibição de exemplo do tabela

```
https://ranking-app.guilouro.now.sh/
```

## Web

Serviço teste para rodar o embed em algum servidor local.

**obs**: Apenas local. 


## Embed

Este é o embed a ser usado.

```html
<div id="ranking-code" 
    campeonato="Brasileirão 2018"
    classifica="6"
    rebaixa="4"
    excludes="GP,GC"
></div>
<script type="text/javascript" src="https://ranking-app.guilouro.now.sh/static/js/main.min.js"></script>
```

#### Legenda:

- **campeonato**: Nome do campeonato
- **classifica**: Número de times que se classificam para a libertadores
- **rebaixa**: Número de times que rebaixam
- **excludes**: Lista de colunas que serão retiradas da exibição da tabela