import React from 'react';
import styled from 'styled-components';

const Table = styled.table`
    width: 100%;
    border-collapse: collapse;
    border: 1px solid #ddd;
    margin-bottom: 10px;
    font-family: sans-serif;
    font-size: 13px;
`;

const Th = styled.th`
    border-bottom: 1px solid #999;
    text-align: left;
    padding: 10px 5px;
    font-size: 10px;
    &:not(:first-child) {
        text-align: center;
    }
`;

const ThTitle = Th.extend`
    font-size: 14px;
    padding: 0 15px;
`;

const Td = styled.td`
    border: 0;
    padding: 5px;
    &:not(:first-child) {
        text-align: center;
    }
    &:nth-child(odd) {
        background-color: #eee;
    }
`;

const TdPontos = Td.extend`
    font-weight: bold;
`;

const TdTime = Td.extend`
    display: flex;
    align-items: center;
    flex-flow: row nowrap;
`;

const TdPosicao = Td.extend`
    text-align: center;
    font-size: 12px;
    ${props => positions(props)}
`;

const positions = (props) => {
    if (props.classifica) {
        return `
            background-color: #91C57B !important;
            color: #fff;
        `
    }

    if (props.rebaixa) {
        return `
            background-color: red !important;
            color: #fff;
        `
    }
}

const Tr = styled.tr`
    &:not(:last-child) {
        border-bottom: 1px dotted #ccc;
    }
`;

const Escudo = styled.img`
    margin-right: 5px;
    max-width: 30px;
`;

export class Tabelas extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tabela: {} };
    }

    componentDidMount() {
        fetch(this.props.url).then(res => res.json()).then(data => {
            this.setState({ tabela: data });
        });
    }

    notFound() {
        return !Object.keys(this.state.tabela).length;
    }

    render() {
        if (this.notFound()) {
            return false;
        }
        
        const { tabela } = this.state;
        const { excludes } = this.props;

        return (
            <article id={this.props.name}>
                <Table>
                    <thead>
                        <Tr>
                            <ThTitle colSpan={!excludes.includes("PO") ? 2 : 1}>
                                {this.props.campeonato}
                            </ThTitle>
                            {!excludes.includes("P") && <Th>P</Th>}
                            {!excludes.includes("J") && <Th>J</Th>}
                            {!excludes.includes("V") && <Th>V</Th>}
                            {!excludes.includes("E") && <Th>E</Th>}
                            {!excludes.includes("D") && <Th>D</Th>}
                            {!excludes.includes("GP") && <Th>GP</Th>}
                            {!excludes.includes("GC") && <Th>GC</Th>}
                            {!excludes.includes("SG") && <Th>SG</Th>}
                        </Tr>
                    </thead>

                    <tbody>
                        {Object.keys(tabela).map((item, id) =>
                            <Tr key={item}>
                                {!excludes.includes("PO") &&
                                    <TdPosicao 
                                        classifica={this.props.classifica && id < this.props.classifica}
                                        rebaixa={this.props.rebaixa && (tabela.length - id) <= this.props.rebaixa}
                                    >
                                        {id+1}º
                                    </TdPosicao>
                                }
                                <TdTime>
                                    <Escudo src={tabela[item].url_logo_equipe} />
                                    {tabela[item].nome_equipe}
                                </TdTime>
                                {!excludes.includes("P") &&<TdPontos>{tabela[item].pontos}</TdPontos>}
                                {!excludes.includes("J") &&<Td>{tabela[item].num_jogos}</Td>}
                                {!excludes.includes("V") &&<Td>{tabela[item].num_vitorias}</Td>}
                                {!excludes.includes("E") &&<Td>{tabela[item].num_empates}</Td>}
                                {!excludes.includes("D") &&<Td>{tabela[item].num_derrotas}</Td>}
                                {!excludes.includes("GP") &&<Td>{tabela[item].gols_pro}</Td>}
                                {!excludes.includes("GC") &&<Td>{tabela[item].gols_sofridos}</Td>}
                                {!excludes.includes("SG") &&<Td>{tabela[item].saldo_gols}</Td>}
                            </Tr>
                        )}
                    </tbody>
                </Table>
            </article>
        )
    }
}

Tabelas.defaultProps = {
    excludes: [],
    campeonato: "",
};

// Schema Tabela

// [{
//     "url_logo_equipe": "",
//     "nome_equipe": "",
//     "pontos": "",
//     "num_jogos": "",
//     "num_vitorias": "",
//     "num_empates": "",
//     "num_derrotas": "",
//     "gols_pro": "",
//     "gols_sofridos": "",
//     "saldo_gols": "",
// }]