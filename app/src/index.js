import React from 'react';
import ReactDOM from 'react-dom';
import { Tabelas } from './Tabela'
// import registerServiceWorker from './registerServiceWorker';

const el = document.getElementById('ranking-code');

ReactDOM.render(
    <Tabelas 
        campeonato="Brasileirão 2018"
        url='https://ranking-api.guilouro.now.sh/'
        classifica={el.getAttribute("classifica") || 6}
        rebaixa={el.getAttribute("rebaixa") || 6}
        excludes={el.getAttribute("excludes") ? el.getAttribute("excludes").split(",") : []}
    />, 
el);

// registerServiceWorker();
